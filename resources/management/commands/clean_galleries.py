#
# Copyright 2020 Martin Owens
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Clean resource galleries that are empty and old.
"""

from datetime import timedelta
from django.utils.timezone import now

from django.core.management import BaseCommand
from django.db.models import Count

from resources.models import Gallery

class Command(BaseCommand):
    help = __doc__

    def handle(self, *args, **options):
        last_month = now() - timedelta(days=30)
        qset = Gallery.objects.annotate(count=Count('items'))
        qset = qset.filter(count=0, created__lt=last_month)
        qset.delete()
