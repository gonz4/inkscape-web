#
# Copyright 2013-2019, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Membership and team admin editing forms.
"""

from typing import List
from django.forms import ModelForm, CharField
from django.utils.translation import ugettext_lazy as _

from djangocms_text_ckeditor.widgets import TextEditorWidget

from .models import Team, TeamMembership, TeamChatRoomType

class TeamForm(ModelForm):
    """Edit a team in the admin interface"""
    class Meta:
        exclude = ('mailman',)
        model = Team

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        # Future: support multiple languages in rooms configuration
        self.lang = 'en'

        for field in ('desc', 'charter', 'side_bar'):
            if field in self.fields:
                self.fields[field].widget = TextEditorWidget()


class TeamEdit(TeamForm):
    """Edit a team, but in the user interface (for team admins)"""
    cr_prefix = 'chat_room_'

    class Meta:
        fields = ('name', 'email', 'icon', 'badge', 'intro', 'desc',
                  'charter', 'side_bar', 'enrole', 'auto_expire')
        model = Team

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)

        existing = dict(self.instance.chatrooms.filter(language=self.lang)\
                                               .values_list('room_type_id', 'channel'))
        self.fields[f'{self.cr_prefix}0'] = \
            CharField(label='IRC', initial=existing.get(None, ''), required=False)

        for chat_type in TeamChatRoomType.objects.all():
            initial = existing.get(chat_type.pk, '')
            self.fields[f'{self.cr_prefix}{chat_type.pk}'] = \
                CharField(label=chat_type.name, initial=initial, required=False)

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        for field, value in self.cleaned_data.items():
            if field.startswith(self.cr_prefix):
                self.save_chat_room(obj, int(field[len(self.cr_prefix):]), value)
        return obj

    def save_chat_room(self, team, room_type_id, value):
        """Save chatroom configuration"""
        room_type = None
        if room_type_id != 0:
            room_type = TeamChatRoomType.objects.get(pk=room_type_id)
        if value:
            team.chatrooms.update_or_create(room_type=room_type, language=self.lang,\
                                            defaults={'channel': value})
        else:
            team.chatrooms.filter(room_type=room_type, language=self.lang).delete()

