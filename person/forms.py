#
# Copyright 2013, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Register users using forms and allow teams and users to edit their data
"""

import re
from collections import OrderedDict
from django.forms import (
    ModelForm, Form, CharField, ModelChoiceField, PasswordInput, ValidationError,
)
from django.db.models import Q
from django.urls import reverse, reverse_lazy
from django.utils.text import format_lazy
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import Permission

from django_registration.forms import RegistrationForm
from nocaptcha_recaptcha.fields import NoReCaptchaField

from .models import User, Team, TeamMembership, SocialMediaSite, UserSocialMedia, EmailBlacklist
from .utils import find_user_photo

# Each pattern is a known spam attack to reject registrations
SPAM_PATTERNS = [
    re.compile(r'^\w+\d+[a-zA-Z]\d+$'),
]

class TeamChoiceField(ModelChoiceField):
    """Display team choices for the user"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.memberships = TeamMembership.objects.none()

    def label_from_instance(self, obj):
        membership = self.memberships.filter(team=obj).first()
        if membership:
            if membership.is_member:
                return obj.name
            if membership.is_requester:
                return format_lazy(_("{team} (request pending)"), team=obj)
        if obj.enrole == 'O':
            return format_lazy(_("{team} (join now)"), team=obj)
        return format_lazy(_("{team} (request membership)"), team=obj)

class PasswordForm(PasswordResetForm):
    """Password reset request form with Recapture"""
    recaptcha = NoReCaptchaField(label=_("Human Test"))
    # These Media entries as bs, unstream should be fixed.
    class Media:
        js = ('https://www.google.com/recaptcha/api.js',)

class RegisForm(RegistrationForm):
    """Registration form with custom User model and NoReCapture"""
    recaptcha = NoReCaptchaField(label=_("Human Test"))

    def clean_email(self):
        """Reject email addresses from a blacklist"""
        email = self.cleaned_data['email']
        server = email.split('@')[-1]
        for item in EmailBlacklist.objects.filter(server__iexact=server):
            raise ValidationError(item.reason)
        return email

    def clean_username(self):
        """Make sure the username isn't already used by someone else"""
        username = self.cleaned_data['username']
        for rex in SPAM_PATTERNS:
            if rex.match(username):
                # This error is delibrately misleading.
                raise ValidationError("Invalid IP Address")
        if len(username) > 60:
            raise ValidationError(_("Username too long"))
        if '/' in username:
            raise ValidationError(_("Username must not include a forward slash '/'."))
        if '@' in username:
            raise ValidationError(_("Username must not be an email address!"))
        return username

    class Meta(RegistrationForm.Meta):
        model = User

    class Media:
        js = ('https://www.google.com/recaptcha/api.js',)

    def save(self, *args, **kwargs):
        obj = super().save(*args, **kwargs)
        find_user_photo(obj)
        return obj

class AgreeToClaForm(Form):
    """Agreement to the CLA allows the community to know we're on the same page"""
    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance')
        super(AgreeToClaForm, self).__init__(*args, **kwargs)

    def save(self):
        """Save the CLA agreement as a user_permission addition"""
        cla = Permission.objects.get(codename='website_cla_agreed')
        self.instance.user_permissions.add(cla)
        return self.instance


class UserForm(ModelForm):
    """User settings, for editing one's own profile"""
    password1 = CharField(label=_('Password'), widget=PasswordInput(), required=False)
    password2 = CharField(label=_('Confirm'), widget=PasswordInput(), required=False)
    ircpass = CharField(widget=PasswordInput(), required=False)
    principle_team = TeamChoiceField(label=_('Principal Team'),
        help_text=format_lazy(_('The team you are most active in, leading or comitted to. <a href="{url}">Join more teams</a>'), url=reverse_lazy('teams')),
        queryset=Team.objects.filter(enrole__in='OPT'), required=False)

    class Meta:
        model = User
        exclude = ('user_permissions', 'is_superuser', 'groups', 'last_login',
                   'is_admin', 'is_active', 'date_joined', 'visits', 'last_seen',
                   'password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_team_field(self.fields['principle_team'])
        self.add_social_media_fields()

    def set_team_field(self, field):
        membership = self.instance.memberships.filter(is_principle=True).first()
        field.memberships = self.instance.memberships.all()
        teams = field.memberships.filter(
            joined__isnull=False,
            expired__isnull=True).values_list('team_id', flat=True)
        field.queryset = Team.objects.filter(Q(enrole__in='OPT') | Q(pk__in=teams))
        if membership is not None:
            field.initial = membership.team

    def add_social_media_fields(self):
        """Add each of the social media fields to this form"""
        self.socfields = OrderedDict()
        for socsite in SocialMediaSite.objects.filter(is_visible=True):
            initial = None
            try:
                initial = socsite.users.get(user=self.instance).socid
            except UserSocialMedia.DoesNotExist:
                pass

            socfield_id = f'socmed__{socsite.pk}'
            self.fields[socfield_id] = CharField(
                label=socsite.name, required=False, initial=initial)
            self.socfields[socfield_id] = socsite

    def fieldsets(self):
        """Split the user settings into different tabs"""
        dat = dict((field.name, field) for field in self)
        yield _("Account Settings"), [dat[k]\
            for k in ['username', 'email', 'password1', 'password2', 'language', 'principle_team']]
        yield _("Personal Details"), [dat[k]\
            for k in ['first_name', 'last_name', 'bio', 'photo', 'gpg_key']]
        yield _("Social Settings"), [dat[k]\
            for k in ['website', 'ircnick',] + list(self.socfields)]

    def clean(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 and password2:
            if password1 != password2:
                raise ValidationError(_("Passwords don't match"))
            self.cleaned_data['password'] = password1
        return self.cleaned_data

    def clean_username(self):
        """Make sure the username isn't already used by someone else"""
        username = self.cleaned_data['username']
        if len(username) > 60:
            raise ValidationError(_("Username too long"))
        if '/' in username:
            raise ValidationError(_("Username must not include a forward slash '/'."))
        elif '@' in username:
            raise ValidationError(_("Username must not be an email address!"))
        user = User.objects.filter(username=username)
        if user and user[0] != self.instance:
            raise ValidationError(_("Username already taken"))
        return username

    def clean_first_name(self):
        """Remove any spaces from names"""
        first_name = self.cleaned_data['first_name']
        first_name = first_name.strip()
        return first_name

    def clean_last_name(self):
        """Remove any spaces from names"""
        last_name = self.cleaned_data['last_name']
        last_name = last_name.strip()
        return last_name

    def save(self, **kwargs):
        """Save the form, making sure to set the password"""
        password = self.cleaned_data.get('password', None)
        if password:
            self.instance.set_password(password)

        # Update principle team membership
        self.instance.memberships.all().update(is_principle=False)
        team = self.cleaned_data['principle_team']
        if team is not None:
            membership = team.get_membership(self.instance)
            if membership is None:
                (membership, msg) = team.join(self.instance, actor=self.instance)
            if membership is not None:
                membership.is_principle = True
                membership.save()

        for (field_id, socsite) in self.socfields.items():
            socid = self.cleaned_data.get(field_id, None)
            if not socid:
                socsite.users.filter(user=self.instance).delete()
            else:
                socsite.users.update_or_create(user=self.instance, defaults={'socid': socid})

        return super().save(self, **kwargs)
