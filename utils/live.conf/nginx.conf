server {
    listen              80;
    listen              443 ssl http2;
    server_name         inkscape.org www.inkscape.org 140.211.15.230;
    root                /var/www/www.inkscape.org;
    expires             30d;

    ssl_certificate     /etc/ssl/nginx/ssl.crt;
    ssl_certificate_key /etc/ssl/nginx/ssl.key;
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_dhparam /etc/ssl/nginx/dhparam.pem; # Generate with openssl dhparam -out /etc/ssl/nginx/certsdhparam.pem 4096
    ssl_ciphers ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384;
    ssl_prefer_server_ciphers off;
    ssl_session_cache   shared:TLS:32m;
    ssl_session_timeout 30m;

    access_log /var/www/www.inkscape.org/data/logs/nginx/access.log;
    error_log /var/www/www.inkscape.org/data/logs/nginx/error.log;

    include /etc/nginx/block_ua.conf;
    if ($bad_client) { return 403; }
    client_max_body_size 120M;

    rewrite ^(.*).php $1.html permanent;

    location /robots.txt { alias /var/www/www.inkscape.org/inkscape/templates/robots.txt; }
    location /favicon.ico { alias /var/www/www.inkscape.org/data/static/favicon.ico; }

    location /static/ {
      alias /var/www/www.inkscape.org/data/static/;
      # Enable CORS for web-fonts via caching service
      if ($request_uri ~* ^.*?\.(eot)|(ttf)|(woff)$){
        add_header Access-Control-Allow-Origin *;
      }
    }

    location /media/ { # MEDIA_URL
      alias /var/www/www.inkscape.org/data/media/;
    }

    location /dl/ { # DOWNLOAD VERSION
      alias /var/www/www.inkscape.org/data/media/;
      if ($request_filename ~ "^.*/(.+)$"){
          set $fname $1;
          add_header Content-Disposition 'attachment; filename="$fname"';
      }
    }

    location / {
        # Redirect here so non-ssl requests to media don't fail
        if ($server_port = 80) {
            return 301 https://inkscape.org$request_uri;
        }
        if ($host != "inkscape.org") {
            return 301 https://inkscape.org$request_uri;
        }
        expires off;
        gzip_static on;
        include uwsgi_params;
        uwsgi_read_timeout 60s;
        uwsgi_connect_timeout 60s;
        uwsgi_send_timeout 60s;
        uwsgi_pass unix:///var/www/www.inkscape.org/data/wsgi.sock;


        # Make MITM downgrade shenanigans harder to pull off
        add_header Strict-Transport-Security "max-age=63072000" always;

        # Stop any click-jacking (use of pages in iframes)
        add_header X-Frame-Options "SAMEORIGIN";

        # A man is never dead so long as his name is spoken.
        add_header X-Clacks-Overhead "GNU Terry Pratchett";


        # Upgrade root for serving upgrade message
        #alias /var/www/www.inkscape.org/static/upgrade/;
    }
}
