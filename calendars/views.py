# -*- coding: utf-8 -*-
#
# Copyright 2020, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Calendars for teams
"""

from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, ListView

from django_ical.utils import build_rrule_from_recurrences_rrule
from django_ical.views import ICalFeed

from person.mixins import TeamMixin

from .models import Event

class NowMixin(object):
    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['now'] = now()
        return data

class TeamEvent(NowMixin, DetailView):
    model = Event

class TeamEventList(NowMixin, TeamMixin, ListView):
    title = _("Calendar")

    def get_parent(self):
        return self.queryset.get(slug=self.kwargs['team'])

    def get_queryset(self):
        return self.get_parent().events.all()

    def get_context_data(self):
        data = super().get_context_data()
        data['team'] = self.get_parent()
        return data

class EventList(NowMixin, ListView):
    title = _("Full Project Calendar")
    model = Event

class TeamEventFeed(TeamMixin, ICalFeed):
    """An ics event feed"""
    product_id = '-//inkscape.org//Calendar//EN'
    timezone = 'UTC'

    def get_object(self, request, team=None):
        self.object = None
        if team is not None:
            self.object = self.queryset.get(slug=team)
        return self.object

    def file_name(self, obj):
        if obj is not None:
            return f"{obj.slug}_cal.ics"
        return f"inkscape_cal.ics"

    def items(self):
        if self.object:
            return self.object.events.order_by('-start')
        return Event.objects.order_by('-start')

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_start_datetime(self, item):
        return item.start

    def item_end_datetime(self, item):
        return item.end

    def item_rrule(self, item):
        """Adapt Event recurrence to Feed Entry rrule."""
        if item.recurrences:
            rules = []
            for rule in item.recurrences.rrules:
                rules.append(build_rrule_from_recurrences_rrule(rule))
            return rules

    def item_exrule(self, item):
        """Adapt Event recurrence to Feed Entry exrule."""
        if item.recurrences:
            rules = []
            for rule in item.recurrences.exrules:
                rules.append(build_rrule_from_recurrences_rrule(rule))
            return rules

    def item_rdate(self, item):
        """Adapt Event recurrence to Feed Entry rdate."""
        return item.exceptions.filter(new_start__isnull=False).values_list('new_start', flat=True)

    def item_exdate(self, item):
        """Adapt Event recurrence to Feed Entry exdate."""
        return item.exceptions.values_list('old_start', flat=True)

